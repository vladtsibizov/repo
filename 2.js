
const isPalindrome = (inputString) => {
  const len = inputString.length;

  for(let i = 0; i < len / 2; i++) {
    if (inputString[i] !== inputString[len - 1 - i]) {
      return false;
    }
  }
  return true;
};

const check = (inputString) => {
  const len = inputString.length;

  if (isPalindrome(inputString)) {
    return 'palindrome';
  }

  for (let i = 0; i < len; i++) {
    for (let j = 0; j < len; j++) {
      let str = '';
      for(let k = 0; k < inputString.length; k++) {
        if (k === i || k === j) {
          continue;
        }
        str += inputString[k];
      }
      if (isPalindrome(str)) {
        return i === j ?
          inputString[i] :
          inputString[i] + ' ' + inputString[j]
      }
    }
  }

  return 'not possible';
};


console.log(check('ffdfsfff'));
